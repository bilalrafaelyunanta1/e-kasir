<?php
    include "../template/header.php"
?>
<form method="post" action="proses_edit.php">
<?php
include "../koneksi.php";
$id = $_GET['id'];
$tampil = mysqli_query($koneksi, "select * from menu where id_menu='$id'");
$hasil = mysqli_fetch_array($tampil);

?>
 <div class="content-wrapper ml-96 w-full h-96 mt-32">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <div class="text-2xl text-center font-bold">Form Edit Menu</div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="p_input_menu.php" method="POST">
                <div class="card-body">
                  <div class="form-group ">
                    <label for="exampleInputEmail1">ID Menu</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan Nama Menu" name="id_menu" value="<?php echo $hasil['id_menu']?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama Menu</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan Nama Menu" name="nama_menu" value="<?php echo $hasil['nama_menu']?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Harga</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Harga" name="harga" value="<?php echo $hasil['harga']?>">
                  </div>
                  <div class="">
                    <label for="exampleInputPassword1">Kategori</label>
                    <select id="" name="kategori" class="form-control" value="<?php echo $hasil['kategori']?>">
                        <option disabled selected>Pilih Kategori</option>
                        <option value="makanan">Makanan</option>
                        <option value="minuman">Minuman</option>
                        <option value="snack">Snack</option>
                    </select>
                  </div>
                  <div class="form-check pt-2">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Centang ajahh</label>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="flex flex-wrap ml-4 my-2">
                  <div class="px-2">
                    <button type="submit" name="simpan" class="bg-blue-600 hover:shadow-xl font-semibold px-3 py-2 rounded-md text-white hover:bg-blue-800">Submit</button>
                  </div>
                  <div class="px-2">
                    <button type="reset" class="bg-blue-600 hover:shadow-xl font-semibold px-4 py-2 rounded-md text-white hover:bg-blue-800">Reset</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card --> 
            </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
</div>
</form>
<?php
 include "../template/footer.php"  
?>