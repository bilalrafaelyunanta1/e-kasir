<?php
    include "../template/header.php"
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <div class="mt-4 mb-4 fs-2 fw-bolder"> Transaksi Makanan dan Minuman
                            <!-- <button type="button" class="btn btn-success my-4 shadow-lg rounded-pill ml-4">Success</button> -->
                        </div>
                        
                        <!-- <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol> -->
                        <!-- <div class="card"> -->
              <!-- <div class="card-header">
                <h3 class="card-title">DataTable with default features</h3>
              </div> -->
              <!-- /.card-header -->
             <div class="flex flex-wrap">
             <!-- <a href="formtambahbuku.php" class="btn btn-primary">Tambah Data Buku</a> -->
              <div class="w-48 ml-auto h-8 rounded-md text-semibold bg-white border-2 border-black">
                <input type="text" placeholder="Search" class="focus:outline-none mx-2 w-40"></input>
              </div>
              <button class="border-2 border-slate-600 rounded-md h-8 ml-2 px-2 hover:bg-slate-600 hover:text-white">Enter</button>
             </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped" style="text-align:center">
                  <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Nama Menu</th>
                    <th>Tanggal Pesan</th>
                    <th>Jumlah Pesan</th>
                    <th>Total Harga</th>
                    <th>Total Bayar</th>
                    <th>Total Kembali</th>
                    <!-- <th>Opsi</th> -->
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                    include "../koneksi.php";
                    $tampil=mysqli_query($koneksi,"select * from transaksi join menu on transaksi.id_menu=menu.id_menu join user on user.id_user=transaksi.id_user");
                    while($data=mysqli_fetch_array($tampil)) {                  
                  ?>
                   <tr>
                    <td><?php echo $data['nama'] ?></td>
                    <td><?php echo $data['nama_menu'] ?></td>
                    <td><?php echo $data['tanggal_pesanan'] ?></td>
                    <td><?php echo $data['jumlah_pesanan'] ?></td>
                    <td><?php echo $data['total_harga'] ?></td>
                    <td><?php echo $data['total_bayar'] ?></td>
                    <td><?php echo $data['total_kembali'] ?></td>
                    <!-- <td class="flex flex-wrap">
                        <button class="border-2 bg-blue-500 font-semibold rounded-md shadow-md mr-1 px-3 py-1 hover:bg-slate-200">Edit</button>
                        <a href="hapus.php?id=<?php // echo $data['id_transaksi'];?>" class="border-2 bg-red-500 font-semibold rounded-md shadow-md mx-1 px-3 py-1 hover:bg-slate-200">Delete</a>
                    <td> -->
                  </tr>
                  <?php
                   }
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
                    </div>
                </main>
                
            </div>
        </div>
       
    </body>
</html>

<?php
 include "../template/footer.php"  
?>